
//A program forrása: https://progpater.blog.hu/2011/02/19/gyonyor_a_tomor

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Készítünk egy binfa strkutúrát ami áll ey értékből és egy bal és jobb oldali pointerből.

typedef struct binfa
{
  int ertek;
  struct binfa *bal_nulla;
  struct binfa *jobb_egy;

//Létrehozzuk a BINFA és BINFA_PTR típusokat.   
} BINFA, *BINFA_PTR;

//Megnézzük hogy ha üres a BINFA_PTR akkor kilépünk. 

BINFA_PTR
uj_elem ()
{
  BINFA_PTR p;

  if ((p = (BINFA_PTR) malloc (sizeof (BINFA))) == NULL)
    {
      perror ("memoria");
      exit (EXIT_FAILURE);
    }
  return p;
}
//Deklaráljuk a kiir és szabadít függvényeket.

extern void kiir (BINFA_PTR elem);
extern void szabadit (BINFA_PTR elem);

int
main (int argc, char **argv)
{
  char b;

  //A gyökérre pédlányosítunk egy elemet ami a '/' lesz ezután ráállítjuk a mutatót a gyökérre 
  
  BINFA_PTR gyoker = uj_elem ();
  gyoker->ertek = '/';
  BINFA_PTR fa = gyoker;

  //Olvassuk a bemenetet a kimentetre (standart output) pedig írunk. 
  while (read (0, (void *) &b, 1))
    {
      write (1, &b, 1);
      //Megnézzük 0-t kel-e tenni a fába 
      if (b == '0')
	{
        //Megnézzük az adott csomópontnak van e nullás gyereke. Ha nincs csinálunk egyet, majd ráállítjuk a mutatót,ezt követően pedig a gyökérre az algoritmus miatt. 
	  if (fa->bal_nulla == NULL)
	    {
	      fa->bal_nulla = uj_elem ();
	      fa->bal_nulla->ertek = 0;
	      fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
            //Ha volt nullás eleme akkor csak ráállítjuk a mutatót. 
	      fa = fa->bal_nulla;
	    }
	}
	//Ez megcsináljuk egyes gyerekkel is. 
      else
	{
	  if (fa->jobb_egy == NULL)
	    {
	      fa->jobb_egy = uj_elem ();
	      fa->jobb_egy->ertek = 1;
	      fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
	      fa = fa->jobb_egy;
	    }
	}
    }
  //Kiirathuk a gyokeret a mélyésggel együtt. 
  printf ("\n");
  kiir (gyoker);
  extern int max_melyseg;
  printf ("melyseg=%d", max_melyseg);
  szabadit (gyoker);
}

static int melyseg = 0;
int max_melyseg = 0;

//A kiír függvényben növeljük megmérjük a mélységet és kiírjuk a fát inorder feldolgozás szerint, tehát először az egyes elemeket, majd a gyökeret utoljára pedig a nullás elemeket. 
void
kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	max_melyseg = melyseg;
      kiir (elem->jobb_egy);
      for (int i = 0; i < melyseg; ++i)
	printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
	      melyseg);
      kiir (elem->bal_nulla);
      --melyseg;
    }
}
//A szabadít függvényben felszabadítjuk az adott elemet a gyermekeivel kezdve. 
void
szabadit (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      szabadit (elem->jobb_egy);
      szabadit (elem->bal_nulla);
      free (elem);
    }
}
