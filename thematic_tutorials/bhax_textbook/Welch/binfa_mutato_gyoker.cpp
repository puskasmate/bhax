// z3a7.cpp
//
// Együtt támadjuk meg: http://progpater.blog.hu/2011/04/14/egyutt_tamadjuk_meg
// LZW fa építő 3. C++ átirata a C valtozatbol (+mélység, atlag és szórás)
// Programozó Páternoszter
//
// Copyright (C) 2011, 2012, Bátfai Norbert, nbatfai@inf.unideb.hu, nbatfai@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Ez a program szabad szoftver; terjeszthetõ illetve módosítható a
// Free Software Foundation által kiadott GNU General Public License
// dokumentumában leírtak; akár a licenc 3-as, akár (tetszõleges) késõbbi
// változata szerint.
//
// Ez a program abban a reményben kerül közreadásra, hogy hasznos lesz,
// de minden egyéb GARANCIA NÉLKÜL, az ELADHATÓSÁGRA vagy VALAMELY CÉLRA
// VALÓ ALKALMAZHATÓSÁGRA való származtatott garanciát is beleértve.
// További részleteket a GNU General Public License tartalmaz.
//
// A felhasználónak a programmal együtt meg kell kapnia a GNU General
// Public License egy példányát; ha mégsem kapta meg, akkor
// tekintse meg a <http://www.gnu.org/licenses/> oldalon.

#include <iostream>		//std::cout kiiratás miatt kell
#include <cmath>		//std::sqrt függvény miatt kell
#include <fstream>		//fájból való beolvasás és kiírás miatt kell

//Lérehozunk egy LZWBinFa osztályt ennek a public részébe létrehozunk egy konstruktort ami ráállítja a fa mutatót a gyökérre.  
//Lérehozunk még egy destrkutort is amiben meghívjuk a szabadit() függvényt a gyoker egyes és nullas gyermekeire 
class LZWBinFa
{
public:
   
    LZWBinFa ()
    {
        gyoker= new Csomopont ('/');
        fa = gyoker;
    }
    ~LZWBinFa ()
    {
        szabadit (gyoker->egyesGyermek ());
        szabadit (gyoker->nullasGyermek ());
        delete(gyoker);
    }
    
    
   

//Tagfüggvényként túlterheljük a << operátort amivel betölthetjük a fába az inputot. A b azt formális paramétert jelöli amit be akarunk a fába tölteni. 
    
    void operator<< (char b)
    {
      // Megnézzük hogy 0-t kell e betenni a fába 
        if (b == '0')
        {
            //Megénzzük az adott csomópontnak van-e 0-s eleme tehát a fa mutató mutat-e ilyenre, ha nincs akkor csinálunk. 
            if (!fa->nullasGyermek ())	
            {
                //A Csomopont osztályból példányosítunk egy új Csomopontontot 0-s paraméterrel 
                Csomopont *uj = new Csomopont ('0');
               //Ezután az adott Csomopont mutatóját rá is állítjuk erre az új 0-s gyerekre 
                fa->ujNullasGyermek (uj);
               //Majd visszaállítjuk a gyökérre az algoritumus miatt 
                fa = gyoker;
            }
            else //Ha van már nullás gyereke az adott Csomopontnak csak ráállítjuk a fa mutatót 			
            {
               
                fa = fa->nullasGyermek ();
            }
        }
        //Ellenkező esetben ha a b nem nulla, tehát 1 akkor ugyanezt eljátszuk csak 1-es gyerekre 
        else
        {
            //Ha nincs gyerek példányosítunk egyet és ráállítjuk a Csomopont mutatóját
            if (!fa->egyesGyermek ())
            {
                Csomopont *uj = new Csomopont ('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            //Ha van akkor csak ráállítja a mutatót 
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }
  
    int getMelyseg (void);


    //A friend globális függvénnyel segítségével fogjuk kiírni a fát, és ezt a kii()-ban meg is hívjuk 
    friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf)
    {
        bf.kiir (os);
        return os;
    }
    void kiir (std::ostream & os)
    {
        melyseg = 0;
        kiir (gyoker, os);
    }

    //létrehozunk a Csomopont osztályt. Ennek a konstruktora alapértelmezetten '/' a gyökér betűvel hozza létre a csomópontot. Ilyet hívunk a fából, aki tagként tartalmazza a gyökeret. Egyébként ha valami betűvel hívjuk azt teszi a betu tagba a két gyermekre mutató mutatót pedig 0-ra állítjuk. 
private:
    class Csomopont
    {
    public:
       
        Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0)
        {
        };
        ~Csomopont ()
        {
        };
        //Megnézzük mi az aktuális csomópont bal oldai (nullas),jobb oldali(egyes) gyerke.
        Csomopont *nullasGyermek () const
        {
            return balNulla;
        }
      
        Csomopont *egyesGyermek () const
        {
            return jobbEgy;
        }
        //Beállítjuk hogy legyen az aktuális csomópontnak gy a bal vagy jobb oldali gyereke
        void ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }
        
        void ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }
        //Itt azt nézzük meg milyeen betűt hordoz a Csomopont
        char getBetu () const
        {
            return betu;
        }

    private:
        //A private részben azt nézzük meg:
        //Milyen betűt hordoz a Csomopont
        char betu;
       //Melyik másik Csomopontnak a bal odali gyereke ha nincs iylen akkor balNulla == null
        Csomopont *balNulla;
        Csomopont *jobbEgy;
       //Ha nem másolható a Csomopont akkor letiltjuk a másoló konstruktort 
        Csomopont (const Csomopont &);
        Csomopont & operator= (const Csomopont &);
    };

   //Mindig az algoritumus szerint megfelelő Csomopontra mutat
    Csomopont *fa;
   
    int melyseg;
    
   //Másolókonstruktor ismételt letiltása
    LZWBinFa (const LZWBinFa &);
    LZWBinFa & operator= (const LZWBinFa &);

   //Kiiratjuk az os-re az aktuális Csomopontot de csak akkor ha létezik az elem, ha nincs Csomopont leállítju a rekurziót. A melysegből levonnunk egyet mert az postorder bejáráshoz képest alapértelmezetten 1-el nagyobb 
    void kiir (Csomopont * elem, std::ostream & os)
    {
        
        if (elem != NULL)
        {
            ++melyseg;
            kiir (elem->egyesGyermek (), os);

            for (int i = 0; i < melyseg; ++i)
                os << "---";
            os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
            kiir (elem->nullasGyermek (), os);
            --melyseg;
        }
    }
    //Felszabadítjuk  Csomopontot,ha létezik, a gyermekeivel kezdve
    void szabadit (Csomopont * elem)
    {
       
        if (elem != NULL)
        {
            szabadit (elem->egyesGyermek ());
            szabadit (elem->nullasGyermek ());
 
            delete elem;
        }
    }

    //A fában a gyökér csomópont ki van tüntetve 
protected:			
    Csomopont *gyoker;
    int maxMelyseg;
    

    void rmelyseg (Csomopont * elem);


};

//Lekérjük a max mélyésget az rmelyseg() függvényben és ezt a getMelyseg() függvényben alakalmazzuk 

int
LZWBinFa::getMelyseg (void)
{
    melyseg = maxMelyseg = 0;
    rmelyseg (gyoker);
    return maxMelyseg - 1;
}





void
LZWBinFa::rmelyseg (Csomopont * elem)
{
    if (elem != NULL)
    {
        ++melyseg;
        if (melyseg > maxMelyseg)
            maxMelyseg = melyseg;
        rmelyseg (elem->egyesGyermek ());
        // ez a postorder bejáráshoz képest
        // 1-el nagyobb mélység, ezért -1
        rmelyseg (elem->nullasGyermek ());
        --melyseg;
    }
}


//Helyes használatot kííró függvény

void
usage (void)
{
    std::cout << "Usage: lzwtree in_file -o out_file" << std::endl;
}

int
main (int argc, char *argv[])
{
    
//Ha nem jól akarjuk használni kiírja  helyes használatot 
   
    if (argc != 4)
    {
       
        usage ();
  
        return -1;
    }


    //Eltároljuk a bemenő fájl nevét 
    char *inFile = *++argv;

  //Van e -o kapcsoló fordításban ha nincs kiírjuk a helyes használatot 
    if (*((*++argv) + 1) != 'o')
    {
        usage ();
        return -2;
    }

    //Ha van a dekalráljuk a a beFile-t amibe az input tartalma van. Ha nins ilyen kiírjuk hogy nem létezik. 
    std::fstream beFile (inFile, std::ios_base::in);

    
    if (!beFile)
    {
        std::cout << inFile << " nem letezik..." << std::endl;
        usage ();
        return -3;
    }
    //Deklaráljuk az output fájlt illetve a b-t amibe a fájlból olvassuk a bájtokat  és a binFa objektumba fogjuk őket írni 
    std::fstream kiFile (*++argv, std::ios_base::out);
    
    unsigned char b;		
    LZWBinFa binFa;		
   
    //Binárisan olvassuk a bemenetet, de a kimenő fájlt karakteresen írjuk ki 
    while (beFile.read ((char *) &b, sizeof (unsigned char)))
        if (b == 0x0a)
            break;

    bool kommentben = false;

    while (beFile.read ((char *) &b, sizeof (unsigned char)))
    {

        if (b == 0x3e)
        {			// > karakter
            kommentben = true;
            continue;
        }

        if (b == 0x0a)
        {			// újsor
            kommentben = false;
            continue;
        }

        if (kommentben)
            continue;

        if (b == 0x4e)		// N betű
            continue;

        //A b-ben lévő bájt bitjeit egyenként megnézzük 
        for (int i = 0; i < 8; ++i)
        {
            //Addig maszkolunk hogy az if fejébe a legmagasabb helyiértétkű bit vizsgáltatát írjuk. 
            if (b & 0x80)
                //Ha a vzisgált bit 1, akkor 1 betűt írunk a binfa objektumba, ha 0 akkor 0-t
                binFa << '1';
            else
                
                binFa << '0';
            b <<= 1;
        }

    }

    
   //A kiFile-ba kiírjuk a fát és a mélységet majd bezárjuk a be és ki fájlokat. 
    kiFile << binFa;		

    kiFile << "depth = " << binFa.getMelyseg () << std::endl;


    kiFile.close ();
    beFile.close ();

    return 0;
}

